﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerScript : MonoBehaviour {

	/*	Move with a/d or left and right arrow. Space to jump.
		change "Move Speed" to make player faster or slower
		.
	*/

	public float JumpSpeed = 10f;
	public float MoveSpeed = 5f;
	public LayerMask GroundCastLayer;
	bool isInAir = false;
	Rigidbody2D rb;
	RaycastHit2D castLeft;
	RaycastHit2D castRight;
	RaycastHit2D castDown;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {

		//Press Space to Jump if player is not in the air
		if(!isInAir)
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				rb.velocity = Vector3.up * JumpSpeed;
			}
		}

		castLeft = Physics2D.Raycast(transform.position + new Vector3(-0.51f,0,0), Vector3.left, 0.01f, GroundCastLayer);
		castRight = Physics2D.Raycast(transform.position + new Vector3(0.51f,0,0), Vector3.right, 0.01f, GroundCastLayer);
		castDown = Physics2D.Raycast(transform.position + new Vector3(0,-0.51f,0), Vector3.down, 0.5f, GroundCastLayer);

		//player move left and right with a,d or left and right arrow
		rb.velocity = new Vector3(MoveSpeed * Input.GetAxisRaw("Horizontal"), rb.velocity.y,0f);

		if(castDown.collider == null)
		{
			isInAir = true;
		}
			else
		{
			isInAir = false;
		}
		//raycast to stop player when next to wall
		if(castLeft.collider != null)
		{
        	if(rb.velocity.x < 0)
        	{
        		rb.velocity = new Vector3(0, rb.velocity.y, 0f);
        	}
        		else
        	{
        		rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0f);
        	}
	    }

        if(castRight.collider != null)
        {
        	if(rb.velocity.x > 0)
        	{
        		rb.velocity = new Vector3(0, rb.velocity.y, 0f);
        	}
        		else
        	{
        		rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, 0f);
        	}
        }
	}

	void OnCollisionEnter2D(Collision2D col2D){

	}
}
